import React from 'react';
import './App.css';


import ChatFrame from './ChatFrame';
import MemberList from './MemberList';
import ChatInput from './ChatInput';

import io from 'socket.io-client';

class App extends React.Component{


	constructor(props) {
    	super(props);
    	this.handleSend = this.handleSend.bind(this);
    	this.handleUsernameSend = this.handleUsernameSend.bind(this);

    	this.state = {
    		messages:[],
    		users:[],
    		socket: io('ws://localhost:5000'),
    	};

    	//const socket = io('ws://localhost:5000');
		this.state.socket.on('connect', () => {
			//socket.emit('setUser', "TEST");

			this.state.socket.on('message', data => {
				let msgText = data.user + ": " + data.message
				this.setState({messages: [...this.state.messages, msgText]})
			});

			this.state.socket.on('addUser', data => {
				console.log(data)
				this.setState({users: [...this.state.users, data]})
			});

			this.state.socket.on('removeUser', data => {
				this.setState({users: this.state.users.filter(user => user !== data)})
			});

		});

	}

	handleSend(message) {
		this.state.socket.emit('message',message)
	}

	handleUsernameSend(message) {
		this.state.socket.emit('username',message)
	}


	render() {
		return (
	  		<div className="App">
	  			<div className="AppTop">
	  				<div className="ChatFrame">
			   	 		<ChatFrame messages={this.state.messages} className="ChatFrame "/>
		   	 		</div>	
			   	 	<div className="ChatList">
			    		<MemberList  members={this.state.users} className="MemberList"/>
		    		</div>
		    	</div>
			    <div className="AppBottom">
	    			<ChatInput className="ChatInput" onSend={this.handleSend} onUsernameSend={this.handleUsernameSend}/>
	    		</div>
	        </div>
		);
	};
}

export default App;

/*
<Container>
	    	<Row>
	    		<Col >
	       	 		<ChatFrame className="ChatFrame"/>
	       	 	</Col>
	       	 	<Col >
	        		<MemberList className="MemberList"/>
	        	</Col>
	        <Row>
	        </Row>
	        	<Col>
	        		<ChatInput className="ChatInput"/>
	        	</Col>
	        </Row>
        </Container>
        */