import React from 'react';

import './ChatInput.css';

import { InputGroup, FormControl }  from 'react-bootstrap'; 

class ChatInput extends React.Component{
	constructor(props) {
	    super(props);
    	this.state = {
    		messagetext: '',
    		username:'User'
    	};

    	//set rndm username

    	this.handleChange = this.handleChange.bind(this);
	    this.handleSend = this.handleSend.bind(this);

	    this.handleUsernameChange = this.handleUsernameChange.bind(this);
	    this.handleUsernameSend = this.handleUsernameSend.bind(this);
	}

	handleChange(event) {    
		this.setState({messagetext: event.target.value});  
	}

	handleSend(event) {
		let message = {
			user: this.state.username,
			message: this.state.messagetext
		}

    	this.props.onSend(message);  
    	this.setState({messagetext:""})
    }

    handleUsernameChange(event) {    
		this.setState({username: event.target.value});  
	}

	handleUsernameSend(event) {
    	this.props.onUsernameSend(this.state.username);  
    }

	render(){
		return (
			<div className="ChatInput">
				<div className="userNameDiv">
					<InputGroup>
							<FormControl value={this.state.username}  onChange={this.handleUsernameChange} />
							<InputGroup.Append>
								<InputGroup.Text id="usernameButton" onClick={this.handleUsernameSend}>Set</InputGroup.Text>
							</InputGroup.Append>
						</InputGroup>
				</div>
				<div className="inputTextDiv">
					<InputGroup className="mb-3">
						<FormControl value={this.state.messagetext}  onChange={this.handleChange}
					placeholder="Type something ..."
					/>
						<InputGroup.Append>
						<InputGroup.Text id="postButton" onClick={this.handleSend}>Send</InputGroup.Text>
						</InputGroup.Append>
					</InputGroup>
				</div>
			</div>
		);
	};
}

export default ChatInput;
