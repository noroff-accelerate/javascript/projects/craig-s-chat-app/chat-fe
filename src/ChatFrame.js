import React from 'react';

function ChatFrame({messages}) {

const listMessages = messages.map((message, i) =>
    <p key={"message-"+i} > {message} </p>
    );

  return (
    <div className="ChatFrame">
    	{ listMessages }
    </div>
  );
}

export default ChatFrame;