import React from 'react';

function MemberList({members}) {

	const listMembers = members.map((member, i) =>
	        <p key={"user-"+i}> {member} </p>
	        );


  return (
    <div className="MemberList">
    	{ listMembers }
    </div>
  );
}


export default MemberList;